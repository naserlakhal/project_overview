import 'package:flutter/material.dart';
import 'package:projects_overview/components/active_project_card.dart';
import 'package:projects_overview/components/bottomNav.dart';
import 'package:projects_overview/components/project_percent.dart';
import 'package:projects_overview/utils/constants.dart';

class HomeScreen extends StatefulWidget {
  const HomeScreen({super.key});

  @override
  State<HomeScreen> createState() => _HomeScreenState();
}

int activeMenu1 = 0;

class _HomeScreenState extends State<HomeScreen> {
  @override
  Widget build(BuildContext context) {
    double widthf = MediaQuery.of(context).size.width;

    return Scaffold(
      body: SafeArea(
        child: Column(
          children: <Widget>[
            Container(
                padding: const EdgeInsets.symmetric(
                    horizontal: 30.0, vertical: 30.0),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.end,
                  children: [
                    Expanded(
                      child: Align(
                        alignment: Alignment.center,
                        child: subheadingTitle('Projects'),
                      ),
                    ),
                    const Icon(Icons.add),
                  ],
                )),
            Column(
              children: [
                Container(
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: List.generate(
                      tabCat.length,
                      (index) {
                        return Padding(
                          padding:
                              const EdgeInsets.only(left: 0, right: 0, top: 30),
                          child: GestureDetector(
                            onTap: () {
                              setState(() {
                                activeMenu1 = index;
                              });
                            },
                            child: Column(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                Text(
                                  tabCat[index],
                                  style: TextStyle(
                                      fontSize: 15,
                                      color: activeMenu1 == index
                                          ? Colors.white
                                          : Colors.grey,
                                      fontWeight: FontWeight.w600),
                                ),
                                activeMenu1 == index
                                    ? Container(
                                        width: 70,
                                        height: 3,
                                        decoration: BoxDecoration(
                                            color: Colors.red,
                                            borderRadius:
                                                BorderRadius.circular(5)),
                                      )
                                    : Container(),
                                const SizedBox(
                                  height: 20,
                                ),
                              ],
                            ),
                          ),
                        );
                      },
                    ),
                  ),
                ),
                const Divider(
                  height: 0,
                  thickness: 2,
                  color: Color.fromARGB(255, 63, 62, 62),
                  indent: 5,
                  endIndent: 8,
                ),
              ],
            ),
            Padding(
              padding: const EdgeInsets.fromLTRB(30, 20, 20, 0),
              child: Row(
                children: [
                  Container(
                    height: 45,
                    width: 250,
                    decoration: BoxDecoration(
                      color: const Color.fromARGB(24, 255, 255, 255),
                      borderRadius: BorderRadius.circular(10),
                    ),
                    child: const TextField(
                      autofocus: true,
                      decoration: InputDecoration(
                          prefixIcon: Icon(Icons.search,
                              color: Color.fromARGB(165, 158, 158, 158)),
                          border:
                              OutlineInputBorder(borderSide: BorderSide.none),
                          hintText: "Search Projects"),
                    ),
                  ),
                  const SizedBox(width: 40),
                  GestureDetector(onTap: () {}, child: filterList())
                ],
              ),
            ),
            Expanded(
              child: SingleChildScrollView(
                child: Column(
                  children: <Widget>[
                    Container(
                      color: Colors.transparent,
                      padding: const EdgeInsets.symmetric(
                          horizontal: 20.0, vertical: 10.0),
                      child: const Column(
                        children: <Widget>[
                          ProjectPercent(
                            icon: Icons.more_vert,
                            iconBackgroundColor: Colors.white38,
                            loadingPercent: 0.76,
                            title: 'Project X',
                            subtitle: 'Due: 06-07-20223',
                          ),
                          SizedBox(
                            height: 15.0,
                          ),
                        ],
                      ),
                    ),
                    Container(
                      color: Colors.transparent,
                      padding: const EdgeInsets.symmetric(
                          horizontal: 20.0, vertical: 10.0),
                      child: const Column(
                        children: <Widget>[
                          ProjectPercent(
                            icon: Icons.more_vert,
                            iconBackgroundColor: Colors.white38,
                            loadingPercent: 0.76,
                            title: 'Project X',
                            subtitle: 'Due: 06-07-20223',
                          ),
                          SizedBox(
                            height: 15.0,
                          ),
                        ],
                      ),
                    ),
                    Container(
                      color: Colors.transparent,
                      padding: const EdgeInsets.symmetric(
                          horizontal: 20.0, vertical: 10.0),
                      child: const Column(
                        children: <Widget>[
                          ProjectPercent(
                            icon: Icons.more_vert,
                            iconBackgroundColor: Colors.white38,
                            loadingPercent: 0.76,
                            title: 'Project X',
                            subtitle: 'Due: 06-07-20223',
                          ),
                          SizedBox(
                            height: 15.0,
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
      bottomNavigationBar: const BottomNav(),
    );
  }
}
