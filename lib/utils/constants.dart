import 'package:flutter/material.dart';

const ProjectOverviewColor = Color.fromARGB(18, 18, 18, 0);

const kPrimaryGradientColor = LinearGradient(
  begin: Alignment.topLeft,
  end: Alignment.bottomRight,
  colors: [
    Color.fromARGB(194, 14, 161, 1),
    Color.fromARGB(244, 109, 65, 1),
  ],
);
Text subheadingTitle(String title) {
  return Text(
    title,
    style: const TextStyle(
        color: Colors.white,
        fontSize: 20.0,
        fontWeight: FontWeight.w700,
        letterSpacing: 1.2),
  );
}

CircleAvatar filterList() {
  return const CircleAvatar(
    radius: 25.0,
    backgroundColor: Color.fromARGB(60, 158, 158, 158),
    child: Icon(
      Icons.filter_list,
      size: 20.0,
      color: Colors.white,
    ),
  );
}

const List tabCat = [
  "Progress",
  "Completed",
];