import 'package:flutter/material.dart';
import 'package:projects_overview/utils/constants.dart';

class BottomNav extends StatelessWidget {
  const BottomNav({super.key});

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: const BoxDecoration(
          gradient: LinearGradient(
              begin: Alignment.topLeft,
              end: Alignment.bottomRight,
              colors: [Color.fromARGB(72, 241, 180, 48), Color.fromARGB(112, 200, 0, 255)])),
      child: BottomNavigationBar(
        elevation: 80,
        backgroundColor: Colors.transparent,
        selectedItemColor: Color.fromARGB(217, 255, 255, 255),
        unselectedItemColor: Color.fromARGB(217, 101, 101, 98),
        items: [
          BottomNavigationBarItem(icon: Icon(Icons.airline_stops), label: ''),
          BottomNavigationBarItem(icon: Icon(Icons.calendar_month), label: ''),
          BottomNavigationBarItem(icon: Icon(Icons.fact_check), label: ''),
        ],
      ),
    );
  }
}
