import 'package:flutter/material.dart';
import 'package:percent_indicator/circular_percent_indicator.dart';

class ProjectPercent extends StatelessWidget {
  final IconData icon;
  final Color iconBackgroundColor;
  final String title;
  final String subtitle;
  final double loadingPercent;

  const ProjectPercent({
    required this.icon,
    required this.iconBackgroundColor,
    required this.title,
    required this.subtitle,
    required this.loadingPercent,
  });

  @override
  Widget build(BuildContext context) {
    return Row(
      crossAxisAlignment: CrossAxisAlignment.center,
      children: <Widget>[
        Padding(
          padding: const EdgeInsets.all(10.0),
          child: CircularPercentIndicator(
            animation: true,
            radius: 30.0,
            percent: loadingPercent,
            lineWidth: 5.0,
            circularStrokeCap: CircularStrokeCap.round,
            backgroundColor: Colors.white10,
            progressColor: Colors.white,
            center: Text(
              '${(loadingPercent * 100).round()}%',
              style: const TextStyle(
                fontWeight: FontWeight.w700,
                color: Colors.white,
              ),
            ),
          ),
        ),
        const SizedBox(width: 10.0),
        Expanded(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Row(
                children: [
                  Expanded(
                    child: Text(
                      title,
                      style: const TextStyle(
                        fontSize: 16.0,
                        fontWeight: FontWeight.w700,
                      ),
                    ),
                  ),
                  Icon(icon), // Icon in the top right corner
                ],
              ),
              Text(
                subtitle,
                style: const TextStyle(
                  fontSize: 14.0,
                  fontWeight: FontWeight.w500,
                  color: Color.fromARGB(115, 155, 155, 155),
                ),
              ),
            ],
          ),
        ),
      ],
    );
  }
}